/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function userInfo() {
	let fullName = prompt("What is your full name?");
	let age = prompt("What is your age?");
	let location = prompt("Which city are you location at?");
	console.log("Hello,",fullName)
	console.log("You are", age, "years old.")
	console.log("You live in", location, "city");
}

userInfo()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
let artistList = () => {
	// let artist1 = prompt("Who is you 1st Favourite Artist");
	// let artist2 = prompt("Who is you 2st Favourite Artist");
	// let artist3 = prompt("Who is you 3st Favourite Artist");
	// let artist4 = prompt("Who is you 4st Favourite Artist");
	// let artist5 = prompt("Who is you 5st Favourite Artist");

	// console.log("1.", artist1)
	// console.log("2.", artist2)
	// console.log("3.", artist3)
	// console.log("4.", artist4)
	// console.log("5.", artist5)

	let artists = ['The Beatles', 'Metallica', 'The Eagles', "L'arc~~en~Ciel", "Eraserheads"];

	for(let i=0; i<artists.length; i++) {
		console.log(i+1 + ". " + artists[i])
	}
	// console.log("1.", artists[0])
	// console.log("2.", artists[1])
	// console.log("3.", artists[2])
	// console.log("4.", artists[3])
	// console.log("5.", artists[4])
}
artistList();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
let movieRatingList = () => {
	let movieRatingObj = {
		"The Godfather": "97%",
		"The Godfather, Part II": "96%",
		"Shawshank Redemption": "91%",
		"To Kill A Mockingbird": "93%",
		"Psycho": "96%",
	}
	// console.log()
	for(let key in movieRatingObj) {
		console.log(key);
		console.log("Rotten Tomatoes Rating: ", movieRatingObj[key])
	}
}
movieRatingList();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);

