console.log("Hello World!");

// [ SECTION ] Syntax, Statements and Comments
	// JS Statements usually end with semicolon (;)
	// JS Statements in programming are instruction that we tell the computer/browser to preform.
	// A syntax in programming, it is the set of rules that describes how statements must be constructed.log

	/*
	There are tow types of comments:
		1. The single-line comment denoted by two slashes //
		2. The multi-line comment denotes by a slash and asterisk 
	*/

// [ SECTION ] variables
	// It is used to contain data
	// Like a storage, container
		// When we create variables, certain portion of a device's memory is given a "name" that we call "variables"
	
	// [ SUB-SECTION ] Declaration Variables
	// Tells our device/local macchine that a variable name is created and is ready to store data.
	// Decclearing a variable without givng it a value will automatically assign it with the value of "undefined", meaning that the variable's value was not yet defined.

		// Syntax
			// let/const variableName;
		let myVaraible;
		console.log(myVaraible); // Print myVariable in the console.

		/*
		Guides in writing variables:
			1. Use "let" keyword followed by the variable name of your choosing and use the assignment operator (=) to assign the value
			2. Variable  names should start with a lowercase character, or also known for camelCase.
				Ex.
				favColor
				favoriteNumber
				collegeCourse
			3. For constant variables, use the "const" keyworld
			4. Variable names should be indicative (or discriptive) of the value being stored to avoid confusion.
		*/

	// [ SUB- SECTION ] Initilizing Variables
		// The instance when a variable is given it's initial/starting value

		// Syntax
			// let/const variableName = value;
		let productName = "desktop computer";
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		// Usually being when a cretain value is not changing.
		// More likely being used for interest rate for a loan, savings account or a mortgage.
		const interest = 3.539;

	// [ SUB-SECION ] Reassigning a variable values 
		// Reassigining a variable pertains to changing it's inital or previous value into another value.

		// Syntax
			// variable = newValue;
		productName = "Laptop"
		console.log(productName)

		// interest = 4.456;
		// console.log(interest);

	// [ var vs. let/const ]
		// var is also used in declaring a variab;e. but var is an ECMAScript1 (ES1) feature, in modern time, ES6 Updates are already now using let/const.

		// For example,
			a = 5;
			console.log(a)
			var a;

		// let keyword,
			// b = 5;
			// console.log(b)
			// let b;

	// [ SUB-SECTION ] let/const local/global scope
		// Scope essentially means where these variables are available for use
		// A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.

			// let outerVariable = "hello";

			// {
			// 	let innerVariable = "hello, again";
			// }

			// console.log(outerVariable);
			// console.log(innerVariable);

			// MiniActivity, solve the code and make sure to print innerVariable

			let outerVariable = "hello";


			let innerVariable = "hello, again";


			console.log(outerVariable);
			console.log(innerVariable);

	// [ SUB-SECTION ] Multiple variables Declerations
		// Multiple variables may be declared in one line.

			let productCode = 'DC017', productBrand = 'Dell';
			console.log(productCode, productBrand);

// [ SECTION ] Data Types
	// [ Strings ] 
		// String is a series of characters that create a word, a phrase, a sentence or anything related to creating text
		// We use either single ('') or double ("") quatation.
		let country = "Philippines";
		let province = " Metro Manila";

		// Concatenating Strings
		// Multiple strings values can be combined to create a single string using the "+" symbol

		let fullAddress = province + ", " + country;
		console.log(fullAddress);

		let greetings = "I live in the " + country;
		console.log(greetings);

		// Escape Character (\) in string in combination with other characters can produce different effects
		// "\n" reffers to creating a new line in between text
		let mailAddress = 'Metro Manial\n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);
		message = 'John\'s employees went home early';
		console.log(message);

	// [ Numbers ]
		// Integer/whole Numbers
		let headcoundt = 26;
		console.log(headcoundt)

		// Decimal Number/Fractions
		let grade = 98.7;
		console.log(grade)

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance)

		// Combining text and strings
		console.log("John's grade last quarter is " + grade);

	// [ Boolean ]
		// Boolean values are normally used to store values relating to the state of certain things
		// This is the, true or false

		let isMarried = false;
		let inGoodConduction = true;

		console.log("isMarried: " + isMarried)
		console.log("inGoodConduction: " + inGoodConduction)

	// [ Arrays ]
		// Arrays are a special kind of data type that's used to store multiple values

		// Syntax
		// let/const arrayName = [elementA, elementB, elementC, ... ]

		let grades = [ 98.7, 92.1, 90.6, 94.6 ];
		console.log(grades)

		// different datatypes
		let details = [ "John", "Smith", 32, true ];
		console.log(details);

	// [ Object ]
		// Objects are another special kind of data type that's used to mimic real world objects/items

		// Syntax
		// let/const objectName = {
		// 	propertyA: value,
		// 	propertyB: value
		// }

		let person = {
			fullName: "Michael Jordan",
			age: 35,
			isMarried: true,
			contact: [ "12345", "67890" ],
			address: {
				houseNumber: "345",
				city: "Chicago"
			}
		}

		console.log(person);

		// typeof operator is used to determine the type of data or value of a variable. It outputs a sring.
		console.log(typeof person);

		// Constant Objects and Arrays

		/*
		The keyword const is a little misleading.
		It does not define a constant value. It defines a constant refference to a value.

		Because of this you can Not:
			Reassign a constant value
			Reassign a constant array
			Reassign a constant object

			But you CAN:
			Change the elements of constant array
			Change the properties of constant object
		*/

		const anime = ['one piece', 'one punch man', 'attack on titan']
		// anime = ['kimetsu no yaiba']
		anime[0] = ['kimetsu no yaiba']

		console.log(anime);

	// [ SECTION ] Null
		// It is used to intentionally express the absence of a value in a variable declaration/initialization.
		// If we use the null value, simple means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

		let spouse = null;
		let myNumber = 
